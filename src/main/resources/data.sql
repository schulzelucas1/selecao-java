INSERT INTO USUARIO(cpf, nome, perfil) VALUES('09484792480', 'Lucas Schulze', 'ADMIN');
INSERT INTO USUARIO(cpf, nome, perfil) VALUES('12345678910', 'André Pereira', 'FUNCIONARIO');
INSERT INTO USUARIO(cpf, nome, perfil) VALUES('09484792480', 'Gabriel Matias', 'CLIENTE');


INSERT INTO ESTADO(nome, sigla, regiao) VALUES('PARAÍBA', 'PB', 'NE');
INSERT INTO ESTADO(nome, sigla, regiao) VALUES('PERNAMBUCO', 'PE', 'NE');

INSERT INTO MUNICIPIO(nome, estado_id) VALUES('JOÃO PESSOA', 1);
INSERT INTO MUNICIPIO(nome, estado_id) VALUES('SANTA RITA', 1);
INSERT INTO MUNICIPIO(nome, estado_id) VALUES('RECIFE', 2);

INSERT INTO BANDEIRA(nome) VALUES('IPIRANGA');
INSERT INTO BANDEIRA(nome) VALUES('PETROBRAS DISTRIBUIDORA S.A.');
INSERT INTO BANDEIRA(nome) VALUES('RAIZEN');
INSERT INTO BANDEIRA(nome) VALUES('BRANCA');

INSERT INTO DISTRIBUIDORA(codigo, nome, municipio_id) VALUES('47930', 'A S DE CASTRO & CIA LTDA', 1);
INSERT INTO DISTRIBUIDORA(codigo, nome, municipio_id) VALUES('47978', 'ADAILTON PEREIRA DA SILVA ME', 1);
INSERT INTO DISTRIBUIDORA(codigo, nome, municipio_id) VALUES('18942', 'ALMEIDA COMERCIO DE DER. DE PETROLEO LTDA', 3);
INSERT INTO DISTRIBUIDORA(codigo, nome, municipio_id) VALUES('18959', 'COMPANHIA BRASILEIRA DE DISTRIBUICAO', 3);

INSERT INTO PRODUTO(tipo, unidade, bandeira_id, distribuidora_id) VALUES('GASOLINA', 'LITRO', 3, 2);
INSERT INTO PRODUTO(tipo, unidade, bandeira_id, distribuidora_id) VALUES('GASOLINA', 'LITRO', 3, 4);

INSERT INTO PRECO(ativo, data_criacao, valor, valor_venda, produto_id) VALUES(true, '2018-01-04',3.969, 5, 1);
INSERT INTO PRECO(ativo, data_criacao, valor, valor_venda, produto_id) VALUES(false, '2018-01-04',2.565, 5, 2);
INSERT INTO PRECO(ativo, data_criacao, valor, valor_venda, produto_id) VALUES(true, '2018-01-05',3.899, 5, 2);

INSERT INTO COMPRA(quantidade, produto_id, usuario_id) VALUES(2, 1, 3);
INSERT INTO COMPRA(quantidade, produto_id, usuario_id) VALUES(5, 2, 3);

