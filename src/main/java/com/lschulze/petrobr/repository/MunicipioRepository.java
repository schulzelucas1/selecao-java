package com.lschulze.petrobr.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lschulze.petrobr.model.Municipio;

public interface MunicipioRepository  extends JpaRepository<Municipio, Long>{

	Municipio findByNome(String municipioNome);

}
