package com.lschulze.petrobr.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lschulze.petrobr.model.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Long>{

}
