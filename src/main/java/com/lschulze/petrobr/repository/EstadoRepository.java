package com.lschulze.petrobr.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lschulze.petrobr.model.Estado;
import com.lschulze.petrobr.model.Regiao;

public interface EstadoRepository extends JpaRepository<Estado, Long>{
	
	List<Estado> findByRegiao(Regiao regiao);
	Estado findBySigla(String sigla);

}
