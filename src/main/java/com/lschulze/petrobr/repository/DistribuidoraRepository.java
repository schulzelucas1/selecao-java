package com.lschulze.petrobr.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lschulze.petrobr.model.Distribuidora;

public interface DistribuidoraRepository extends JpaRepository<Distribuidora, Long>{
	Distribuidora findByCodigo(Long codigo);
}
