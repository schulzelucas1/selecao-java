package com.lschulze.petrobr.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lschulze.petrobr.model.Preco;

public interface PrecoRepository extends JpaRepository<Preco, Long>{

	List<Preco> findByAtivo(boolean ativo);
	
}
