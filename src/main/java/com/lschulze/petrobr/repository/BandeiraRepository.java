package com.lschulze.petrobr.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lschulze.petrobr.model.Bandeira;

public interface BandeiraRepository extends JpaRepository<Bandeira, Long>{

	Bandeira findByNome(String nome);

}
