package com.lschulze.petrobr.model;

public enum Perfil {

	ADMIN,
	FUNCIONARIO,
	CLIENTE
}
