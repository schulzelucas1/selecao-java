package com.lschulze.petrobr.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Usuario {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String cpf;
	private String nome;
	@Enumerated(EnumType.STRING)
	private Perfil perfil;
	@OneToMany(mappedBy = "usuario")
	private List<Compra> compras = new ArrayList<>();
	
	public Usuario(String cpf, String nome, Perfil perfil) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.perfil = perfil;
	}
	
	
}
