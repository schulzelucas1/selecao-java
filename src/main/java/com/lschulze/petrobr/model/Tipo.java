package com.lschulze.petrobr.model;

public enum Tipo {

	DIESEL,
	ETANOL,
	GASOLINA,
	GNV
}
