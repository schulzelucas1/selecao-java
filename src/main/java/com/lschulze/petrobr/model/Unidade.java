package com.lschulze.petrobr.model;

public enum Unidade {

	LITRO,
	METRO_CUBICO
}
