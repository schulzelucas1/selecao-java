package com.lschulze.petrobr.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Preco {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private double valor;
	private double valorVenda;
	private boolean ativo = true;
	private LocalDateTime dataColeta;
	private LocalDateTime dataCriacao = LocalDateTime.now();
	@ManyToOne
	private Produto produto;
	
	
	public Preco(double valor, double valorVenda, LocalDateTime dataColeta, Produto produto) {
		this.valor = valor;
		this.valorVenda = valorVenda;
		this.dataColeta = dataColeta;
		this.produto = produto;
	}

}
