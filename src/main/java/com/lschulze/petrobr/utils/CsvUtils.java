package com.lschulze.petrobr.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.multipart.MultipartFile;

public class CsvUtils {

	    public static List<String> read(MultipartFile file) throws IOException {
	     	BufferedReader br;
	    	List<String> result = new ArrayList<>();
	    	try {

	    	     String line;
	    	     InputStream is = file.getInputStream();
	    	     br = new BufferedReader(new InputStreamReader(is));
	    	     while ((line = br.readLine()) != null) {
	    	          result.add(line);
	    	     }

	    	  } catch (IOException e) {
	    	    System.err.println(e.getMessage());       
	    	  }
			return result;
	  	    }
	    
	    public static List<String> convertRow(String row)
	    {
	    	row = row.replaceAll("   ",";");
	        String[] rowArr = row.split("  |;");
	        List<String> result = Arrays.stream(rowArr).collect(Collectors.toList());
	        for (String string : result) {
				string = string.trim();;
			}
	        return result;
	    }

		public static boolean validacaoParse(List<String> valores) {
			try {
				Double.valueOf(valores.get(7).replaceAll(",","."));
				Double.valueOf(valores.get(8).replaceAll(",","."));
				return true;
			} catch (Exception e) {
				return false;
			}
		}
}
