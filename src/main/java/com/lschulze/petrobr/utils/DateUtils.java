package com.lschulze.petrobr.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtils {
	
	public static LocalDateTime converteDate(String date) {
		System.out.println();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy"); 
		LocalDateTime formatDateTime = LocalDate.parse(date, formatter).atStartOfDay();
		return formatDateTime;
	}
}
