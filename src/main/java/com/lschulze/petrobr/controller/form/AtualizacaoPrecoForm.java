package com.lschulze.petrobr.controller.form;

import com.lschulze.petrobr.model.Preco;
import com.lschulze.petrobr.model.Produto;
import com.lschulze.petrobr.repository.PrecoRepository;
import com.lschulze.petrobr.repository.ProdutoRepository;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class AtualizacaoPrecoForm {
	
	private float valor;
	private Long produtoId;
	
	public Preco atualizar(Long id, PrecoRepository precoRepository, ProdutoRepository produtoRepository) {
		Preco preco = precoRepository.getOne(id);
		Produto produto = produtoRepository.getOne(produtoId);
		preco.setValor(this.valor);
		preco.setProduto(produto);
		
		return preco;
	}

}
