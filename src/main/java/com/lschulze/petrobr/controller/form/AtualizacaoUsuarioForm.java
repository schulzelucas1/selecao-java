package com.lschulze.petrobr.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.lschulze.petrobr.model.Perfil;
import com.lschulze.petrobr.model.Usuario;
import com.lschulze.petrobr.repository.UsuarioRepository;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class AtualizacaoUsuarioForm {
	
	@NotNull @NotEmpty
	private String cpf;
	@NotNull @NotEmpty
	private String nome;
	private Perfil perfil;
	
	public Usuario atualizar(Long id, UsuarioRepository usuarioRepository) {
		Usuario usuario = usuarioRepository.getOne(id);
		usuario.setCpf(this.cpf);
		usuario.setNome(this.nome);
		usuario.setPerfil(this.perfil);
		
		return usuario;
	}

}
