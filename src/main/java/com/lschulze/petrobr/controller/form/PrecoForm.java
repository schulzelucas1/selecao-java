package com.lschulze.petrobr.controller.form;

import java.time.LocalDateTime;

import com.lschulze.petrobr.model.Preco;
import com.lschulze.petrobr.model.Produto;
import com.lschulze.petrobr.repository.ProdutoRepository;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PrecoForm {
	
	private float valor;
	private float valorVenda;
	private LocalDateTime dataColeta;
	private Long produtoId;
	
	
	public Preco converter(ProdutoRepository produtoRepository) {

		Produto produto = produtoRepository.getOne(produtoId);
		return new Preco(valor, valorVenda, dataColeta, produto);
	}
}
