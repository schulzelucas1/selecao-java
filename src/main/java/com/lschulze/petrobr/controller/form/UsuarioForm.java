package com.lschulze.petrobr.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.lschulze.petrobr.model.Perfil;
import com.lschulze.petrobr.model.Usuario;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class UsuarioForm {

	@NotNull @NotEmpty
	private String cpf;
	@NotNull @NotEmpty
	private String nome;
	private Perfil perfil;
	
	public Usuario converter() {
		return new Usuario(cpf, nome, perfil);
	}
}
