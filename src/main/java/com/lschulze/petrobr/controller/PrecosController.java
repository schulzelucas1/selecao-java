package com.lschulze.petrobr.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.lschulze.petrobr.controller.dto.DetalharPrecoDto;
import com.lschulze.petrobr.controller.dto.PrecoDto;
import com.lschulze.petrobr.controller.form.AtualizacaoPrecoForm;
import com.lschulze.petrobr.controller.form.PrecoForm;
import com.lschulze.petrobr.model.Preco;
import com.lschulze.petrobr.model.Usuario;
import com.lschulze.petrobr.repository.PrecoRepository;
import com.lschulze.petrobr.repository.ProdutoRepository;

@RestController
@RequestMapping("/precos")
public class PrecosController {
	
	@Autowired
	private PrecoRepository precoRepository;
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	@GetMapping
	public List<PrecoDto> lista() {
		List<Preco> precos = precoRepository.findAll();
		return PrecoDto.converter(precos);
	}
	
	@PostMapping
	@Transactional
	public ResponseEntity<PrecoDto> cadastrar(@RequestBody @Valid PrecoForm form, UriComponentsBuilder uriBuilder) {
		
		List<Preco> precos = precoRepository.findByAtivo(true);
		
		for (Preco preco : precos) {
			if(preco.getProduto().getId() == form.getProdutoId())
			preco.setAtivo(false);
		}
		
		Preco preco = form.converter(produtoRepository);
		precoRepository.save(preco);
		
		URI uri = uriBuilder.path("/precos/{id}").buildAndExpand(preco.getId()).toUri();
		return ResponseEntity.created(uri).body(new PrecoDto(preco));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<DetalharPrecoDto> detalhar(@PathVariable Long id) {
		Optional<Preco> preco = precoRepository.findById(id);
		if(preco.isPresent()) {
			return ResponseEntity.ok(new DetalharPrecoDto(preco.get()));
		}
		return ResponseEntity.notFound().build();
	}
	
	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<PrecoDto> atualizar(@PathVariable Long id, @RequestBody @Valid AtualizacaoPrecoForm form){
		Optional<Preco> optional = precoRepository.findById(id);
		if(optional.isPresent()) {
			Preco preco = form.atualizar(id, precoRepository, produtoRepository);
			return ResponseEntity.ok(new PrecoDto(preco));
		}
		return ResponseEntity.notFound().build();	
	}
	
	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<?> remover(@PathVariable Long id)
	{
		Optional<Preco> optional = precoRepository.findById(id);
		if(optional.isPresent()) {
			precoRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}
	
}
