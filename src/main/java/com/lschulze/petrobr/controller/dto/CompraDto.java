package com.lschulze.petrobr.controller.dto;

import com.lschulze.petrobr.model.Compra;
import com.lschulze.petrobr.model.Produto;
import com.lschulze.petrobr.model.Usuario;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class CompraDto {
	
	private Long id;
	private Produto produto;
	private Usuario usuario;
	private int quantidade;
	
	public CompraDto(Compra compra) {
		this.id = compra.getId();
		this.produto = compra.getProduto();
		this.usuario = compra.getUsuario();
		this.quantidade = compra.getQuantidade();
	}

	
}
