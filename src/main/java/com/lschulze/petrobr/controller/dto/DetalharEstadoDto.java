package com.lschulze.petrobr.controller.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;

import com.lschulze.petrobr.model.Estado;
import com.lschulze.petrobr.model.Municipio;
import com.lschulze.petrobr.model.Regiao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class DetalharEstadoDto {

	
	private Long id;
	private String sigla;
	private String nome;
	private Regiao regiao;
	private List<Municipio> municipios = new ArrayList<>();
	
	public DetalharEstadoDto(Estado estado) {
		this.id = estado.getId();
		this.sigla = estado.getSigla();
		this.nome = estado.getNome();
		this.regiao = estado.getRegiao();
		this.municipios = estado.getMunicipios();
	}
}
