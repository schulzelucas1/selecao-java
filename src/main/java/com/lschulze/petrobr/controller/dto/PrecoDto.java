package com.lschulze.petrobr.controller.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.lschulze.petrobr.model.Preco;
import com.lschulze.petrobr.model.Produto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class PrecoDto {

	private Long id;
	private double valor;
	private double valorVenda;
	private boolean ativo;
	private LocalDateTime dataColeta;
	private LocalDateTime dataCriacao;
	private String produtoNome;
	
	public static List<PrecoDto> converter(List<Preco> precos) {
		List<PrecoDto> precosDto = new ArrayList<PrecoDto>();
		for (Preco preco : precos) {
			precosDto.add(new PrecoDto(preco.getId(), preco.getValor(),preco.getValorVenda(), preco.isAtivo(), preco.getDataColeta(), preco.getDataCriacao(), preco.getProduto().getTipo().name()));
		}
		return precosDto;
	}
	
	public PrecoDto(Preco preco) {
		this.id = preco.getId();
		this.valor = preco.getValor();
		this.valorVenda = preco.getValorVenda();
		this.ativo = preco.isAtivo();
		this.dataColeta = preco.getDataColeta();
		this.dataCriacao = preco.getDataCriacao();
		this.produtoNome = preco.getProduto().getTipo().name();
	}
}
