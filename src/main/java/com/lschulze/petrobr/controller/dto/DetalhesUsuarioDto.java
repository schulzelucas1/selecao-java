package com.lschulze.petrobr.controller.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.lschulze.petrobr.model.Compra;
import com.lschulze.petrobr.model.Perfil;
import com.lschulze.petrobr.model.Usuario;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class DetalhesUsuarioDto {

	private Long id;
	private String cpf;
	private String nome;
	private Perfil perfil;
	private List<CompraDto> compras = new ArrayList<>();
	
	public DetalhesUsuarioDto(Usuario usuario) {
		super();
		this.id = usuario.getId();
		this.cpf = usuario.getCpf();
		this.nome = usuario.getNome();
		this.perfil = usuario.getPerfil();
		this.compras = new ArrayList<>();
		this.compras.addAll(usuario.getCompras().parallelStream().map(CompraDto::new).collect(Collectors.toList()));
		
	}
	
	
	
}
