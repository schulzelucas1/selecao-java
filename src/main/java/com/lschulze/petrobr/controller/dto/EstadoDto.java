package com.lschulze.petrobr.controller.dto;

import java.util.ArrayList;
import java.util.List;

import com.lschulze.petrobr.model.Estado;
import com.lschulze.petrobr.model.Municipio;
import com.lschulze.petrobr.model.Regiao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class EstadoDto {

	private Long id;
	private String sigla;
	private String nome;
	private Regiao regiao;
	private List<MunicipioDto> municipios = new ArrayList<>();
	
	
	public static List<EstadoDto> converter(List<Estado> estados) {
		List<EstadoDto> estadosDto = new ArrayList<EstadoDto>();
		for (Estado estado : estados) {
			estadosDto.add(new EstadoDto(estado.getId(), estado.getSigla(), estado.getNome(), estado.getRegiao(), MunicipioDto.converter(estado.getMunicipios())));
		}
		return estadosDto;
	}


	public EstadoDto(Estado estado) {
		this.id = estado.getId();
		this.sigla = estado.getSigla();
		this.nome = estado.getNome();
		this.regiao = estado.getRegiao();
	}
}
