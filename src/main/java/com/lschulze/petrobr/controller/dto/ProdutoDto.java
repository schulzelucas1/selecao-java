package com.lschulze.petrobr.controller.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.lschulze.petrobr.model.Bandeira;
import com.lschulze.petrobr.model.Preco;
import com.lschulze.petrobr.model.Produto;
import com.lschulze.petrobr.model.Tipo;
import com.lschulze.petrobr.model.Unidade;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class ProdutoDto {
	
	private Long id;
	private String tipo;
	private LocalDateTime dataCriacao;
	private String unidade;
	private BandeiraDto bandeira;
	private List<PrecoDto> precos = new ArrayList<>();

	
	public static List<ProdutoDto> converter(List<Produto> produtos) {
		List<ProdutoDto> produtosDto = new ArrayList<>();
		for (Produto produto : produtos) {
			produtosDto.add(new ProdutoDto(produto.getId(), produto.getTipo().name(), produto.getDataCriacao(), produto.getUnidade().name(), new BandeiraDto(produto.getBandeira()),PrecoDto.converter(produto.getPrecos())));
		}
		return produtosDto;
	}

	public ProdutoDto(Produto produto) {
		this.id = produto.getId();
		this.tipo = produto.getTipo().name();
		this.dataCriacao = produto.getDataCriacao();
		this.unidade = produto.getUnidade().name();
		this.bandeira = new BandeiraDto(produto.getBandeira());
		this.precos = PrecoDto.converter(produto.getPrecos());

	}
}
