package com.lschulze.petrobr.controller.dto;

import java.util.ArrayList;
import java.util.List;

import com.lschulze.petrobr.model.Perfil;
import com.lschulze.petrobr.model.Usuario;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class UsuarioDto {

	private Long id;
	private String cpf;
	private String nome;
	private Perfil perfil;


	public static List<UsuarioDto> converter(List<Usuario> usuarios) {
		List<UsuarioDto> usuariosDto = new ArrayList<UsuarioDto>();
		for (Usuario usuario : usuarios) {
			usuariosDto.add(new UsuarioDto(usuario.getId(), usuario.getCpf(), usuario.getNome(), usuario.getPerfil()));
		}
		return usuariosDto;
	}


	public UsuarioDto(Usuario usuario) {
		this.id = usuario.getId();
		this.cpf = usuario.getCpf();
		this.nome = usuario.getNome();
		this.perfil = usuario.getPerfil();
	}
}
